#!/bin/bash

git config --global user.email "julia@pkgserver.com" 
git config --global user.name "JuliaPkgServer" 

julia /PkgServer/01-make_registry.jl

julia /PkgServer/02-run-server.jl
