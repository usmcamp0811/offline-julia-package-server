#################
# run_server.jl #
#################

using LocalPackageServer

config = LocalPackageServer.Config(Dict(
    # Server parameters
    "host"           => "0.0.0.0",
    "port"           => 8000,
    # "pkg_server"     => "https://127.0.0.1:8000",
    "pkg_server"     => "https://pkg.julialang.org",

    # This is where persistent data will be stored
    # (I use the current directory here; adjust to your constraints)
    "local_registry" => abspath("local-registry.git"), # In accordance with the preliminary step above
    "cache_dir"      => abspath("cache"),
    "git_clones_dir" => abspath("data"),
))

# The tricky part: arrange for the server to never update its registries
# when it is offline
if get(ENV, "LOCAL_PKG_SERVER_OFFLINE", "0") == "1"
    @info "Running offline => no registry updates"
    config.min_time_between_registry_updates = typemax(Int)
end

# Start the server
LocalPackageServer.start(config)
