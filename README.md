# Offline Julia Package Server

The purpose of this repository is to make it easy for people to be able to use Julia on an offline network (one that does not connect to the internet). 
I have created a simple Docker Image that will launch a Local Package Server that will create a local cache of packages requested of it while connected
to the internet. For offline use one just builds the Docker Image with the cache inside of it, or other wise copies the files to the offline network. 

## Online Setup

- Build Image:
```
docker build -t offline_package_server .
```

- Run Server:
```
docker run -it -v $PWD:/PkgServer --restart always --name JuliaPkgServer -e LOCAL_PKG_SERVER_OFFLINE=0 -p 8000:8000 offline_package_server:latest
```

- Cache Packages:
Any new package installed in this way will cause your computers Julia Package Manager to request packages from the Server we just created.
This will result in the caching of said package(s). 
```
JULIA_PKG_SERVER=http://<localhost/packge server ip>:8000 julia
# Install some packages
using Pkg; Pkg.add("DataFrames")
```

## Offline Use

- **Optional: Rebuild Image with cached packages in it**
`docker build -t offline_package_server .`

- Copy Docker Image / Cached Packages to Offline Network

- Run Server: 
```
docker run -it -v $PWD:/PkgServer --restart always --name JuliaPkgServer -e LOCAL_PKG_SERVER_OFFLINE=1 -p 8000:8000 offline_package_server:latest
```

- Install Packages as needed
```
JULIA_PKG_SERVER=http://<localhost/packge server ip>:8000 julia
```


*Credit: I want to recognize François Févotte for helping me get the Package Server working offline. His response to my [StackOverlfow Question](https://stackoverflow.com/questions/64730013/how-to-make-julia-pkgserver-jl-work-offline) was crucial to me getting this to work.*

