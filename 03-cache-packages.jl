using Pkg
Pkg.add("Gumbo")
Pkg.add("Cascadia")

function cache_package(pkg::String)
#=
A simple function to cache a given package. 
=#
    Pkg.activate(pkg)
    Pkg.add(pkg)
    Pkg.instantiate()
    rm(pkg, recursive=true)
end

# cache_package("DifferentialEquations")


using Gumbo
using Cascadia

pkg_url = "https://juliaobserver.com/packages"
cat_url = "https://juliaobserver.com/categories"
# url = "https://juliaobserver.com/categories/Data%20Science"
urls = [
    "https://juliaobserver.com/categories/Graphics",
    "https://juliaobserver.com/categories/Data%20Science",
    "https://juliaobserver.com/categories/File%20Io",
    "https://juliaobserver.com/categories/Physics",
    "https://juliaobserver.com/categories/Database",
    "https://juliaobserver.com/categories/Graphical%20Plotting",
    "https://juliaobserver.com/categories/Dev%20Ops",
    "https://juliaobserver.com/categories/Web",
    "https://juliaobserver.com/categories/DISCRETEMATH",
    "https://juliaobserver.com/categories/Space%20Science",
    "https://juliaobserver.com/categories/General%20Differential%20Equations",
    "https://juliaobserver.com/categories/R",
    "https://juliaobserver.com/categories/Jupyter",
    "https://juliaobserver.com/categories/Terminal",
    "https://juliaobserver.com/categories/Machine%20Learning",
    "https://juliaobserver.com/categories/Super%20Computing",
    ]
pkg_list = []
for url in urls
    page = parsehtml(read(download(url), String))
    packages = eachmatch(sel".list-group-item-heading", page.root)
    @show packages = [text(pkg) for pkg in packages]
    push!(pkg_list, (url,packages))
    for pkg in packages
        try
            cache_package(String(pkg))
        catch
            println("Failed: $pkg")
        end
    end
end


println(pkg_list)
